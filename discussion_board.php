<?php

/*
    PAGE NAME: discussion_board.php
    PURPOSE: Gives users the ability to post and review other members posts.
*/

session_start();

$title ="DISCUSSION BOARD";
require_once("assets/includes/header.php");

if (isset($_SESSION["username"])) {

$start = 0;
$limit = 5;
	
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$limit = ($id + 5);
}
else
	$id = 1;
	
	if (isset($_POST["submit"])) {
		$subject_text = mysqli_real_escape_string($conn, stripslashes($_POST["subject"]));
		$post_text = mysqli_real_escape_string($conn, str_replace("\r\n", "<br />", $_POST["post"]));
		$post_insert = "INSERT INTO posts VALUES (DEFAULT, '$member_id', DEFAULT, '$subject_text', '$post_text')";
		
		if ($conn->query($post_insert) === TRUE) {
			$last_id = mysqli_insert_id($conn);
			
			$email_query = mysqli_query($conn, "SELECT email FROM members WHERE membership_status = 'Enabled' AND email_status = 'Enabled'");
			$recipients = array();
			
			while($email_row = $email_query->fetch_assoc()) {
				$recipients[] = $email_row['email'];
			}

			$name_query = mysqli_query($conn, "SELECT member_id, last_name, first_name FROM members WHERE member_id = '$member_id'");
			$name_row = $name_query->fetch_assoc();
			
			if ($member_id == 0)
				$name = "Admin";
			else
				$name = $name_row["first_name"] . " " . $name_row["last_name"];
			
			$to = "info@selectcounsel.law";
			$subject = "New Discussion Post: " . $subject_text;
			
			$post = "<html><body>"; 
			$post .= "<p>The following topic was recently posted to the Select Counsel Discussion Board:" . "</p>";
			$post .= "<p>Subject: " . $subject_text . "</p>";
			$post .= "<p>Name: " . $name . "</p>";
			$post .= "<p>Post: " . $post_text . "</p>";
			$post .= "<p>Respond Now: http://selectcounsel.law/member_area/post.php?post_id=" . $last_id . "</p>";
			$post .= "</body></html>";
			
			$headers = "From: Select Counsel <info@selectcounsel.law>" . "\r\n" ;
			$headers .= "Reply-To: info@selectcounsel.law" . "\r\n";
			$headers .= "BCC: " . implode(", ", $recipients) . "\r\n";
			$headers .= "MIME-Version: 1.0" . "\r\n" ;
			$headers .= "Content-Type: text/html; charset=ISO-8859-1" . "\r\n" ;
			
			mail($to, $subject, $post, $headers);
			
			echo "<script>window.open('discussion_board.php#main-separator','_self')</script>";
		}
		else
			echo $conn->error;
	}
?>
		
		<div id="top-bbox" class="visible-sm visible-md visible-lg">
			<div id="blue-bbox">
				<h3 class="panel-header">Tips & Updates</h3>
			</div>
			<div id="wood-bbox">
				<p>For networking best practices, keep your profile current. <a href="update_profile.php">Update Profile</a> &gt;</p>
			</div>
		</div>
		
		<div class="hidden-xs hidden-sm">
			<h3 class="panel-header">Welcome to the Member Portal</h3>
			<p id="panel-text">The Select Counsel members-only portal is the place to engage with other members, access helpful information, and manage your membership. Use the discussion board below to post questions and topics or facilitate referrals and networking opportunities. (Please don't post your personal blogs.) You can use the navigation bar on the left to learn about membership perks and benefits, and update your profile.</p>
		</div>
		
		<div id="main-separator" class="main-separator"></div>
		
		<ul id="logged-in" class="hidden-lg hidden-md">
			<li class="reg">Logged In As</li>
			<li class="semi"><?php echo $fn . ' ' . $ln; ?></li>
		</ul>
		
		<form action="discussion_board.php" method="POST" role="form" data-toggle="validator">
			<input class="form-control panel-input" type="text" name="subject" placeholder="Enter Subject..." required /><br />
			<textarea class="form-control panel-textarea" rows="5" name="post" placeholder="Enter Post..." required></textarea><br />
			<button id="panel-submit" type="submit" class="btn btn-primary" name="submit" onclick="return confirm('Are you sure you want to submit post?')">Submit</button><br />
		</form>
		
		<div class="main-separator"></div>
		
		<?php
		$posts_query = "SELECT *, members.member_id, posts.post_id, COUNT(comments.comment) AS comment_count, UNIX_TIMESTAMP(posts.datetime) AS datetime FROM posts
							LEFT JOIN members ON members.member_id = posts.member_id
							LEFT JOIN comments ON comments.post_id = posts.post_id
							GROUP BY posts.post_id
							ORDER BY posts.datetime DESC
							LIMIT $start, $limit";
		$posts_tbl = $conn->query($posts_query);
		
		while ($posts_row = $posts_tbl->fetch_assoc()) {
			echo "<div class='row posts'>";
			echo "<div class='col-xs-3 col-sm-2 portrait-bbox'>";
			
			if ($posts_row["member_id"] == 0)
				echo "<img class='portrait-img' src='../assets/portrait_images/placeholder.jpg' />";
			else
				echo "<a href='../profile.php?member_id=" . $posts_row['member_id'] . "' target='_blank'><img class='portrait-img' src='../assets/portrait_images/" . $posts_row["image_path"] . "' /></a>";
			
			echo "</div>";
			echo "<div class='col-xs-9 col-sm-10 post-bbox'>";
			if ($posts_row["member_id"] == 0)
				echo "<p class='name'>Admin</p>";
			else
				echo "<p class='name'>" . $posts_row["first_name"] . " " . $posts_row["last_name"] . "</p>";
			echo "<p class='city'>" . $posts_row["metro_area"] . " " . $posts_row["city"] . "</p>";
			echo "<p class='date'>" . date("F j, Y", $posts_row["datetime"]) . "</p>";
			echo "<p class='subject'>" . $posts_row["subject"] . "</p>";
			$post_text = substr($posts_row["post"], 0, 200);
			
			// The Regular Expression filters
			$url_pattern = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
			$email_pattern = "/[a-z0-9_\-\+]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i";
			
			// Check if there is a url in the text
			if (preg_match($url_pattern, $post_text, $url)) {
				// make the urls hyper links
				echo preg_replace($url_pattern, '<a href="'.$url[0].'" rel="nofollow">'.$url[0].'</a>', "<p class='post readmore'>" . $post_text);
			}
			else if (preg_match($email_pattern, $post_text, $email)) {
				echo preg_replace($email_pattern, '<a href="mailto:'.$email[0].'" rel="nofollow">'.$email[0].'</a>', "<p class='post readmore'>" . $post_text);
			}
			else {
				 // if no urls in the text just return the text
				echo "<p class='post readmore'>" . $post_text;	
			}
			
			if (strlen($posts_row["post"]) >= 200)
				echo "... <a class='morelink' href='post.php?post_id=" . $posts_row["post_id"] . "'>Read More</a></p>"; 
			else
				echo "</p>";
			echo "<p class='comment hidden-xs'>" . $posts_row["comment_count"] . " <a href='post.php?post_id=" . $posts_row["post_id"] . "'>Comment(s)</a> <img class='comment-img' src='assets/images/comment-square.svg' /><a class='comment-link' href='post.php?post_id=" . $posts_row["post_id"] . "'>Add Comment</a></p>";
			echo "<p class='comment visible-xs'>" . $posts_row["comment_count"] . " <a href='post.php?post_id=" . $posts_row["post_id"] . "'>Comment(s)</a><br /><img class='comment-img' src='assets/images/comment-square.svg' /><a class='comment-link' href='post.php?post_id=" . $posts_row["post_id"] . "'>Add Comment</a></p>";
			echo "</div>";
			echo "</div>";
			echo "<hr class='post-separator' />";
		}
		?>
		
		<div id="current-post" class="current-post"></div>
		
		<div class="result"></div>
		
		<?php
		if ($posts_tbl->num_rows > 0) {		
			// fetch all the data from database.
			$rows = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM posts
															LEFT JOIN members ON members.member_id = posts.member_id
															LEFT JOIN comments ON comments.post_id = posts.post_id
															GROUP BY posts.post_id"));
		
			// calculate total page number for the given table in the database 
			$total = ceil($rows / $limit);
			?>

				<div class='row'>
					<?php
					// Show next 5
					if ($id <= $rows)
						echo "<a id='show-more'>Show More</a>";
					?>
				</div>
		
			<?php	
			}
			?>
	</div>
</div>

<?php
}
else
	echo "<script>window.open('../member_login.php','_self')</script>";
?>

<script>
	$(document).ready(function() {
	
		$('.panel-input').keypress(function(e) {
	    	if ( e.which == 13 ) e.preventDefault();
	 	});
	 	
	 	var start = 5;
	 	var limit = 5;
	 	
	 	$('#show-more').click(function() {
		 	start = start;
		 	limit = limit + 5;
		 	console.log(start);
		 	console.log(limit);
			$.ajax({
			    type: "GET",
			    url: "db_data.php",
			    data: { start: start, limit: limit},
			    success: function(data) {
					$( ".result" ).html(data);
				}
			});
		});
	 	
	});
</script>

<?php
require_once("assets/includes/footer.php");
?>