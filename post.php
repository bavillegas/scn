<?php

/*
    PAGE NAME: post.php
    PURPOSE: Gives users the ability to see entirety of a post and comment on it.
*/

session_start();

$title ="POST";
require_once("assets/includes/alt_header.php");

if (isset($_SESSION["username"])) {
	$start = 0;
	$limit = 5;
	
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		$limit = ($id + 5);
	}
	else
		$id = 1;
	
	$post_id = $_GET["post_id"];
	
	if (isset($_POST["submit"])) {
		$member_id = $_POST["member_id"];
		$poster_id = $_POST["poster_id"];
		$subject = mysqli_real_escape_string($conn, stripslashes($_POST["subject"]));
		$comment_text = mysqli_real_escape_string($conn, str_replace("\r\n", "<br />", $_POST["comment"]));
		$comment_insert = "INSERT INTO comments VALUES (DEFAULT, '$member_id', '$post_id', DEFAULT, '$comment_text')";
		
		if ($conn->query($comment_insert) === TRUE) {
			$comment_text = str_replace("\r\n", '', $comment_text);
			$last_id = mysqli_insert_id($conn);
			
			$email_query = mysqli_query($conn, "SELECT email FROM members WHERE member_id = '$poster_id'");
			$email_row = $email_query->fetch_assoc();
			
			$poster_email = $email_row["email"];
			
			$name_query = mysqli_query($conn, "SELECT member_id, email, last_name, first_name FROM members WHERE member_id = '$member_id'");
			$name_row = $name_query->fetch_assoc();
			
			if ($member_id == 0)
				$name = "Admin";
			else
				$name = $name_row["first_name"] . " " . $name_row["last_name"];
			
			$commenter_email = $name_row["email"];
			
			$to = $poster_email;
			$subject = "Comment Re: " . $subject;
			
			$post = "<html><body>"; 
			$post .=  "<p>The following comment was recently posted to the Select Counsel Discussion Board:" . "</p>";
			$comment .= "<p>Name: " . $name . "</p>";
			$comment .= "<p>Comment: " . $comment_text . "</p>";
			$comment .= "<p>Respond Now: http://selectcounsel.law/member_area/post.php?post_id=" . $post_id . "</p>";
			$comment .= "</body></html>";
			
			$headers = "From: " . $name . " <" . $commenter_email . ">\r\n" ;
			$headers .= "Reply-To: " . $commenter_email . "\r\n";
			$headers .= "CC: info@selectcounsel.law" . "\r\n";
			$headers .= "MIME-Version: 1.0" . "\r\n" ;
			$headers .= "Content-Type: text/html; charset=ISO-8859-1" . "\r\n" ;
			
			mail($to, $subject, $comment, $headers);
			
			echo "<script>window.open('post.php?post_id=" . $post_id . "#main-separator','_self')</script>";
		}
		else
			echo $conn->error;
	}
?>
	
		<ul id="logged-in" class="hidden-lg hidden-md">
				<li class="reg">Logged In As</li>
				<li class="semi"><?php echo $fn . ' ' . $ln; ?></li>
		</ul>

<?php
		$posts_query = "SELECT *, members.member_id, posts.post_id, COUNT(comments.comment) AS comment_count, UNIX_TIMESTAMP(posts.datetime) AS datetime FROM posts
							LEFT JOIN members ON members.member_id = posts.member_id
							LEFT JOIN comments ON comments.post_id = posts.post_id
							WHERE posts.post_id = '$post_id'
							ORDER BY posts.datetime DESC";
		$posts_tbl = $conn->query($posts_query);
		
		while($prow = $posts_tbl->fetch_assoc()) {
			echo "<div class='row posts'>";
			echo "<div class='col-xs-3 col-sm-2'>";
			
			if ($prow["member_id"] == 0)
				echo "<img class='portrait-img' src='../assets/portrait_images/placeholder.jpg' />";
			else
				echo "<a href='../profile.php?member_id=" . $prow["member_id"] . "' target='_blank'><img class='portrait-img' src='../assets/portrait_images/" . $prow["image_path"] . "' /></a>";
			
			echo "</div>";
			echo "<div class='col-xs-9 col-sm-10 post-bbox'>";
			
			if ($prow["member_id"] == 0)
				echo "<p class='name'>Admin</p>";
			else
				echo "<p class='name'>" . $prow["first_name"] . " " . $prow["last_name"] . "</p>";
			
			echo "<p class='city'>" . $prow["metro_area"] . " " . $prow["city"] . "</p>";
			echo "<p class='date'>" . date("F j, Y", $prow["datetime"]) . "</p>";
			echo "<p class='subject'>" . $prow["subject"] . "</p>";
			
			// The Regular Expression filters
			$url_pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
			$email_pattern = "/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/i";
			
			// Check if there is a url in the text
			if (preg_match($url_pattern, $prow["post"], $url)) {
				// make the urls hyper links
				echo preg_replace($url_pattern, '<a href="'.$url[0].'" rel="nofollow">'.$url[0].'</a>', "<p class='post readmore'>" . $prow["post"] . "</p>");
			}
			else if (preg_match($email_pattern, $prow["post"], $email)) {
				echo preg_replace($email_pattern, '<a href="mailto:'.$email[0].'" rel="nofollow">'.$email[0].'</a>', "<p class='post readmore'>" . $prow["post"] . "</p>");
			}
			else {
				 // if no urls in the text just return the text
				echo "<p class='post readmore'>" . $prow["post"] . "</p>";
			}
			
			//echo "<p class='post readmore'>" . $prow["post"] . "</p>";
			echo "<p class='comment hidden-xs'>" . $prow["comment_count"] . " <a href='post.php?post_id=" . $prow["post_id"] . "#main-separator'>Comment(s)</a> <img class='comment-img' src='assets/images/comment-square.svg' /><a class='comment-link' href='post.php?post_id=" . $prow["post_id"] . "#comment-bbox'>Add Comment</a></p>";
			echo "<p class='comment visible-xs'>" . $posts_row["comment_count"] . " <a href='post.php?post_id=" . $posts_row["post_id"] . "#main-separator'>Comment(s)</a><br /><img class='comment-img' src='assets/images/comment-square.svg' /><a class='comment-link' href='post.php?post_id=" . $posts_row["post_id"] . "#comment-bbox'>Add Comment</a></p>";
			echo "</div>";
			echo "</div>";
			
			$_SESSION["poster_id"] = $prow["member_id"];
			$_SESSION["subject"] = $prow["subject"];
		}
		?>
		
		<div id="main-separator" class="main-separator"></div>
		
		<?php
		$comments_query = "SELECT *, members.member_id, posts.post_id, comments.comment_id, UNIX_TIMESTAMP(comments.datetime) AS datetime FROM comments
								LEFT JOIN members ON members.member_id = comments.member_id
								LEFT JOIN posts ON posts.post_id = comments.post_id
								WHERE comments.post_id = '$post_id'
								GROUP BY comments.comment_id
								ORDER BY comments.datetime DESC
								LIMIT $start, $limit";
		$comments_tbl = $conn->query($comments_query);
			
		if ($comments_tbl->num_rows > 0) {		
			// fetch all the data from database.
			$rows = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM comments
															LEFT JOIN members ON members.member_id = comments.member_id
															LEFT JOIN posts ON posts.post_id = comments.post_id
															WHERE comments.post_id = '$post_id'
															GROUP BY comments.comment_id"));
		
			// calculate total page number for the given table in the database 
			$total = ceil($rows / $limit);
			
			$_SESSION["post_id"] = $post_id;
			$session_post_id = $_SESSION["post_id"];
			?>

				<div class='row'>
					<?php
					// Show next 5
					if ($id != $total)
						echo "<a id='previous_comments' href='?post_id=" . $session_post_id . "&id=" . ($id + 5) . "#current-comment'>Show previous comments</a>";
					?>
				</div>
		
		<?php	
		}
		
		while($crow = $comments_tbl->fetch_assoc()) {
			echo "<div class='row comments'>";
			echo "<div class='col-xs-3 col-sm-2 comment-imgs'>";
			if ($crow["member_id"] == 0)
				echo "<img class='portrait-img' src='../assets/portrait_images/placeholder.jpg' />";
			else
				echo "<a href='../profile.php?member_id=" . $crow["member_id"] . "' target='_blank'><img class='portrait-img' src='../assets/portrait_images/" . $crow["image_path"] . "' /></a>";
			echo "</div>";
			echo "<div class='col-xs-9 col-sm-10 comment-text'>";
			if ($crow["member_id"] == 0)
				echo "<p class='name'>Admin</p>";
			else
				echo "<p class='name'>" . $crow["first_name"] . " " . $crow["last_name"] . "</p>";
			echo "<p class='city'>" . $crow["metro_area"] . " " . $crow["city"] . "</p>";
			echo "<p class='date'>" . date("F j, Y", $crow["datetime"]) . "</p>";
			
			// The Regular Expression filters
			$url_pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
			$email_pattern = "/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})/i";
			
			// Check if there is a url in the text
			if (preg_match($url_pattern, $crow["comment"], $url)) {
				// make the urls hyper links
				echo preg_replace($url_pattern, '<a href="'.$url[0].'" rel="nofollow">'.$url[0].'</a>', "<p class='post readmore'>" . $crow["comment"] . "</p>");
			}
			else if (preg_match($email_pattern, $crow["comment"], $email)) {
				echo preg_replace($email_pattern, '<a href="mailto:'.$email[0].'" rel="nofollow">'.$email[0].'</a>', "<p class='post readmore'>" . $crow["comment"] . "</p>");
			}
			else {
				 // if no urls in the text just return the text
				echo "<p class='post readmore'>" . $crow["comment"] . "</p>";
			}
			
			//echo "<p class='post readmore'>" . $crow["comment"] . "</p>";
			echo "</div>";
			echo "</div>";
    	}
    	
    	$cphoto_query = "SELECT * FROM members WHERE member_id = '$member_id'";
		$cphoto_tbl = $conn->query($cphoto_query);	
		
		$cprow = $cphoto_tbl->fetch_assoc();
    	?>
    	
    	<div id="current-comment"></div>
    	
    	<div id="cphoto" class='row'>
			<div id="cphoto-bbox" class='col-xs-3 col-sm-2'>
			<?php
			if ($cprow["member_id"] == 0)
				echo "<img class='portrait-img' src='../assets/portrait_images/placeholder.jpg' />";
			else
				echo "<a href='../profile.php?member_id=" . $cprow["member_id"] . "' target='_blank'><img class='portrait-img' src='../assets/portrait_images/" . $cprow["image_path"] . "' /></a>";
			?> 
			</div>
			<div id="comment-bbox" class='col-xs-9 col-sm-10'>
				<form action='post.php?post_id=<?php echo $post_id; ?>' method='POST' role='form' data-toggle='validator'>
					<textarea class='form-control panel-textarea' rows='5' name='comment' placeholder='Enter Comment...' required /></textarea><br />
					<input type='hidden' name='member_id' value='<?php echo $member_id ?>' />
					<input type='hidden' name='poster_id' value='<?php echo $_SESSION["poster_id"] ?>' />
					<input type='hidden' name='subject' value='<?php echo $_SESSION["subject"] ?>' />
					<button id='panel-submit' type='submit' class='btn btn-primary' name='submit' onclick="return confirm('Are you sure you want to submit comment?')">Submit</button><br />
				</form>
			</div>
		</div>
	</div>
</div>

<?php
}
else
	echo "<script>window.open('../member_login.php','_self')</script>";

require_once("assets/includes/footer.php");
?>