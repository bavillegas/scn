<?php

/*
    PAGE NAME: member_login.php
    PURPOSE: Gives users the ability to login to the member portal.
*/

session_start();
	
$title ="MEMBERS PANEL";
require_once("member_area/assets/includes/login_header.php");

if (isset($_POST["member_login"])) {
	$username = $_POST["username"];
	$password = $_POST["password"];
	$password = sha1($password);
	
	$username = mysqli_real_escape_string($conn, $username);
	$password = mysqli_real_escape_string($conn, $password);
	
	$sel_user = "SELECT * FROM admins WHERE username = '$username' AND password = '$password'";
	$run_user = $conn->query($sel_user);
	$check_user = mysqli_num_rows($run_user);
	
	if ($check_user > 0) {
		$_SESSION["username"] = $username;
			
		$row = $run_user->fetch_assoc();
		$_SESSION["admin_id"] = $row["admin_id"];
		$_SESSION["last_name"] = $row["last_name"];
		$_SESSION["first_name"] = $row["first_name"];
			
		echo "<script>window.open('member_area/discussion_board.php','_self')</script>";
	}
	else {
		$sel_user = "SELECT * FROM members WHERE email = '$username' AND password = '$password'";
		$run_user = $conn->query($sel_user);
		$check_user = mysqli_num_rows($run_user);
		$row = $run_user->fetch_assoc();
			
		if ($row["password"] == 'a5bfdbd6cb743d601d7fe90d9ca7c32b24fa352d') {
			$_SESSION["username"] = $username;
			echo "<script>window.open('member_area/change_password.php','_self')</script>";
		}
		else {
			if ($check_user > 0) {
				$_SESSION["username"] = $username;		
				$_SESSION["member_id"] = $row["member_id"];
				$_SESSION["last_name"] = $row["last_name"];
				$_SESSION["first_name"] = $row["first_name"];
						
				echo "<script>window.open('member_area/discussion_board.php','_self')</script>";
			}
			else
				echo "<script>alert('Username or password is incorrect, please try again!')</script>";
		}
	}
}

if (isset($_SESSION['username']) && $_SESSION['username'] == true && isset($_SESSION['last_name']) && $_SESSION['last_name'] == true && isset($_SESSION['first_name']) && $_SESSION['first_name'] == true)
	echo "<script>window.open('member_area/discussion_board.php','_self')</script>";
?>

<div id="member-login-container">
<a href="../index.php"><img id="login-logo" src="assets/images/sc_logo.png" /></a>
<h3>Member Portal</h3>
<form action="member_login.php" method="POST" data-toggle="validator">
	<input class="member-input" type="text" class="form-control" name="username" placeholder="Username or Email" required autofocus><br />
	<input class="member-input" type="password" class="form-control" name="password" placeholder="Password" required><br />
	<input id="member-submit" type="submit" class="form-control" name="member_login" value="LOG IN">
</form>
	<a id="not-a-member" href="member_application.php">Not a Member? Apply Now</a>&nbsp;&nbsp;&nbsp;<span id="separator"><br />
	<a id="not-a-member" href="member_area/reset_password.php">Forgot Password? Reset Now</a>&nbsp;&nbsp;&nbsp;<span id="separator">
</div>

<?php
require_once("member_area/assets/includes/login_footer.php");
?>