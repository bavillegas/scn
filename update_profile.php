<?php

/*
    PAGE NAME: update_profile.php
    PURPOSE: Gives users the ability to update their profile.
*/

session_start();

$title ="UPDATE PROFILE";
require_once("assets/includes/header.php");

if (isset($_SESSION["username"])) {	
?>			
		<ul id="logged-in" class="hidden-lg hidden-md">
			<li class="reg">Logged In As</li>
			<li class="semi"><?php echo $fn . ' ' . $ln; ?></li>
		</ul>
		
		<div class="hidden-xs">
			<h3 class="panel-header">Update Your Profile</h3>
			<p id="panel-text">Keep the network and counsel-seekers up to date with a current profile. The more information you provide, the easier it is for members and legal prospects to find you.</p>
		</div>
		
		<div class="main-separator"></div>
		
		<?php
		$members_query = "SELECT * FROM members WHERE member_id = '$member_id'";
		$members_tbl = $conn->query($members_query);
		
		$m_row = $members_tbl->fetch_assoc();
		?>
		
		<form id="update-profile" action="results.php" method="POST" role="form" data-toggle="validator">
			<input class="form-control panel-input" type="text" name="first_name" value="<?php echo $m_row['first_name']; ?>" placeholder="First Name (required)" required /><br />
			<input class="form-control panel-input" type="text" name="last_name" value="<?php echo $m_row['last_name']; ?>" placeholder="Last Name (required)" required /><br />
			<input class="form-control panel-input" type="text" name="email" value="<?php echo $m_row['email']; ?>" placeholder="Email (required)" required /><br />
			<input class="form-control panel-input" type="text" name="phone" value="<?php echo $m_row['phone']; ?>" placeholder="Phone (required)" required /><br />
			<input class="form-control panel-input" type="text" name="website" value="<?php echo $m_row['website']; ?>" placeholder="Website (required)" required /><br />
			<input class="form-control panel-input" type="text" name="linkedin" value="<?php echo $m_row['linkedin']; ?>" placeholder="LinkedIn (required)" required /><br />
			<input class="form-control panel-input" type="text" name="current_firm" value="<?php echo $m_row['current_firm']; ?>" placeholder="Current Firm (required)" required /><br />
			
			<select class="selectpicker panel-select" name="past_firms[]" data-width="100%" multiple data-max-options="3" title="Past Am Law 200 Firms (Select up to 3 options) (required)" data-header="Past Firms" required>	
				<?php
				$past_firms_query_a = "SELECT * FROM past_firms 
										INNER JOIN menu_data_firms ON past_firms.firm_id = menu_data_firms.firm_id
										WHERE member_id = '$member_id'";
				$past_firms_tbl_a = $conn->query($past_firms_query_a);	
				
				while($pf_row_a = $past_firms_tbl_a->fetch_assoc())
					echo "<option value='" . $pf_row_a["firm_id"] . "' selected>" . $pf_row_a["firm"] . "</option>";
				
				$past_firms_query_b = "SELECT * FROM menu_data_firms ORDER BY firm ASC";
				$past_firms_result_b = $conn->query($past_firms_query_b);
				
				while($pf_row_b = $past_firms_result_b->fetch_assoc())
					echo("<option value='" . $pf_row_b["firm_id"] . "'>" . $pf_row_b["firm"] . "</option>");
				?>
			</select><br />
			
			<input class="form-control panel-input" type="text" name="total_years" value="<?php echo $m_row['total_years']; ?>" placeholder="Total Years Spent at Big Law (for internal records, not displayed on profile)" /><br />
			
			<select id="practice_area_1" class="selectpicker panel-select" name="practice_area_1" data-width="100%" title="Practice Area (required)" data-header="Practice Area" required>
				<?php
				$pa_query_a = "SELECT * FROM practice_areas 
								INNER JOIN menu_data_practice_areas ON practice_areas.practice_area_id = menu_data_practice_areas.practice_area_id 
								WHERE group_id = '1' AND member_id = '$member_id'";
				$pa_result_a = $conn->query($pa_query_a);
				$pa_row_a = $pa_result_a->fetch_assoc();
				
				echo "<option value='" . $pa_row_a["practice_area_id"] . "' selected>" . $pa_row_a["practice_area"] . "</option>";
						
				$pa_query_b = "SELECT * FROM menu_data_practice_areas ORDER BY practice_area ASC";
				$pa_result_b = $conn->query($pa_query_b);
				
				while($pa_row_b = $pa_result_b->fetch_assoc())
					echo "<option value='" . $pa_row_b["practice_area_id"] . "'>" . $pa_row_b["practice_area"] . "</option>";
				?>
			</select><br />
			
			<select id='subcategory_1' class='selectpicker panel-select pull-right' name='subcategory_1[]' data-width='95%' multiple title='Practice Area Subcategories (select all that apply)' data-header="Practice Area Subcategories">
				<?php
				if (mysqli_num_rows($pa_result_a) != 0) {
					$sc_query_a = "SELECT * FROM subcategories 
									INNER JOIN menu_data_subcategories ON subcategories.subcategory_id = menu_data_subcategories.subcategory_id
									WHERE group_id = '1' AND member_id = '$member_id'";
					$sc_result_a = $conn->query($sc_query_a);
				
					while($sc_row_a = $sc_result_a->fetch_assoc())
						echo "<option value='" . $sc_row_a["subcategory_id"] . "' selected>" . $sc_row_a["subcategory"] . "</option>";
					
					$pa_id_1 = $pa_row_a["practice_area_id"];
					
					$sc_query_b = "SELECT * FROM menu_data_subcategories
									WHERE practice_area_id = '$pa_id_1' 
									ORDER BY subcategory ASC";

					$sc_result_b = $conn->query($sc_query_b);
			
					while($sc_row_b = $sc_result_b->fetch_assoc())
						echo "<option value='" . $sc_row_b["subcategory_id"] . "'>" . $sc_row_b["subcategory"] . "</option>";
				}
				?>
			</select>
			
			<select id="practice_area_2" class="selectpicker panel-select" name="practice_area_2" data-width="100%" multiple data-max-options="1" title="Practice Area (optional)" data-header="Practice Area">
				<?php
				$pa_query_c = "SELECT * FROM practice_areas 
								INNER JOIN menu_data_practice_areas ON practice_areas.practice_area_id = menu_data_practice_areas.practice_area_id 
								WHERE group_id = '2' AND member_id = '$member_id'";
				$pa_result_c = $conn->query($pa_query_c);
				$pa_row_c = $pa_result_c->fetch_assoc();
				
				echo "<option value='" . $pa_row_c["practice_area_id"] . "' selected>" . $pa_row_c["practice_area"] . "</option>";
					
				$pa_query_d = "SELECT * FROM menu_data_practice_areas ORDER BY practice_area ASC";
				$pa_result_d = $conn->query($pa_query_d);
				
				while($pa_row_d = $pa_result_d->fetch_assoc())
					echo "<option value='" . $pa_row_d["practice_area_id"] . "'>" . $pa_row_d["practice_area"] . "</option>";
				?>
			</select><br />
			
			<select id="subcategory_2" class='selectpicker panel-select pull-right' name='subcategory_2[]' data-width='95%' multiple title='Practice Area Subcategories (select all that apply)' data-header="Practice Area Subcategories">
				<?php
				if (mysqli_num_rows($pa_result_c) != 0) {
					$sc_query_c = "SELECT * FROM subcategories 
									INNER JOIN menu_data_subcategories ON subcategories.subcategory_id = menu_data_subcategories.subcategory_id
									WHERE group_id = '2' AND member_id = '$member_id'";
					$sc_result_c = $conn->query($sc_query_c);
					
					while($sc_row_c = $sc_result_c->fetch_assoc())
						echo "<option value='" . $sc_row_c["subcategory_id"] . "' selected>" . $sc_row_c["subcategory"] . "</option>";
						
					$pa_id_2 = $pa_row_c["practice_area_id"];
					
					$sc_query_d = "SELECT * FROM menu_data_subcategories
									WHERE practice_area_id = '$pa_id_2' 
									ORDER BY subcategory ASC";
	
					$sc_result_d = $conn->query($sc_query_d);
				
					while($sc_row_d = $sc_result_d->fetch_assoc())
						echo "<option value='" . $sc_row_d["subcategory_id"] . "'>" . $sc_row_d["subcategory"] . "</option>";
				}
				?>
			</select>
			
			<select id="practice_area_3" class="selectpicker panel-select" name="practice_area_3" data-width="100%" multiple data-max-options="1" title="Practice Area (optional)" data-header="Practice Area">
				<?php
				$pa_query_e = "SELECT * FROM practice_areas 
								INNER JOIN menu_data_practice_areas ON practice_areas.practice_area_id = menu_data_practice_areas.practice_area_id 
								WHERE group_id = '3' AND member_id = '$member_id'";
				$pa_result_e = $conn->query($pa_query_e);
				$pa_row_e = $pa_result_e->fetch_assoc();
				
					echo "<option value='" . $pa_row_e["practice_area_id"] . "' selected>" . $pa_row_e["practice_area"] . "</option>";
					
				$pa_query_f = "SELECT * FROM menu_data_practice_areas ORDER BY practice_area ASC";
				$pa_result_f = $conn->query($pa_query_f);
				
				while($pa_row_f = $pa_result_f->fetch_assoc())
					echo "<option value='" . $pa_row_f["practice_area_id"] . "'>" . $pa_row_f["practice_area"] . "</option>";
				?>
			</select><br />
			
			<select id="subcategory_3" class='selectpicker panel-select pull-right' name='subcategory_3[]' data-width='95%' multiple title='Practice Area Subcategories (select all that apply)' data-header="Practice Area Subcategories">
				<?php
				if (mysqli_num_rows($pa_result_e) != 0) {
					$sc_query_e = "SELECT * FROM subcategories 
									INNER JOIN menu_data_subcategories ON subcategories.subcategory_id = menu_data_subcategories.subcategory_id
									WHERE group_id = '3' AND member_id = '$member_id'";
					$sc_result_e = $conn->query($sc_query_e);
					
					while($sc_row_e = $sc_result_e->fetch_assoc())
						echo "<option value='" . $sc_row_e["subcategory_id"] . "' selected>" . $sc_row_e["subcategory"] . "</option>";	
					
					$pa_id_3 = $pa_row_e["practice_area_id"];
					
					$sc_query_f = "SELECT * FROM menu_data_subcategories
									WHERE practice_area_id = '$pa_id_3' 
									ORDER BY subcategory ASC";
	
					$sc_result_f = $conn->query($sc_query_f);
					
					while($sc_row_f = $sc_result_f->fetch_assoc())
						echo "<option value='" . $sc_row_f["subcategory_id"] . "'>" . $sc_row_f["subcategory"] . "</option>";
				}
				?>
				</select>
			
			<textarea class="form-control alt-panel-textarea" rows="4" name="industry" placeholder="Industry (100 max. word count, separate industry names by commas - e.g. tech, life sciences, construction, etc.)" maxlength="1000"><?php echo $m_row['industry']; ?></textarea><br />
			
			<textarea class="form-control panel-textarea" rows="4" name="licenses" placeholder="List applicable state and federal licenses (separate licenses by commas - e.g. California, New York, Northern District of California, US Court of Appeals (9th Circuit), etc.) (required)" required><?php echo $m_row['licenses']; ?></textarea><br />
			
			<select class="selectpicker panel-select" name="law_school" data-width="100%" title="Law School (required)" data-header="Law School" required>
				<option value="<?php echo $m_row['law_school'] ?>" selected><?php echo $m_row['law_school'] ?></option>
				<?php
				// Select content from db to populate metro_area drop-down list
				$law_school_query = "SELECT * FROM menu_data_law_schools ORDER BY law_school ASC";
				$law_school_result = $conn->query($law_school_query);
				
				// Output data of each row
				while($row = $law_school_result->fetch_assoc())
					echo("<option value='" . urlencode($row["law_school"]) . "'>" . $row["law_school"] . "</option>");
				?>
			</select><br />
			
			<input class="form-control panel-input" type="text" name="graduation_year" value="<?php echo $m_row['graduation_year']; ?>" placeholder="Law School Graduation Year ‘YYYY’ (required)" required /><br />
			
			<input class="form-control panel-input" type="text" name="advanced_degrees" value="<?php echo $m_row['advanced_degrees']; ?>" placeholder="Advanced Degrees" /><br />
			
			<input class="form-control panel-input" type="text" name="honors_and_awards" value="<?php echo $m_row['honors_and_awards']; ?>" placeholder="Honors and Awards (100 word count, separate by commas)" /><br />
			
			<input class="form-control panel-input" type="text" name="roles" value="<?php echo $m_row['roles']; ?>" placeholder="Notable Roles/Experience" /><br />
			
			<select class="selectpicker panel-select" name="state" data-width="100%" title="State" data-header="State">
				<option value="<?php echo $m_row['state'] ?>" selected><?php echo $m_row['state'] ?></option>
				<option value="">NO STATE</option>
				<?php
				// Select content from db to populate state drop-down list
				$state_query = "SELECT * FROM menu_data_states ORDER BY state ASC";
				$state_result = $conn->query($state_query);
				
				// Output data of each row
				while($row = $state_result->fetch_assoc())
					echo("<option value='" . urlencode($row["state"]) . "'>" . $row["state"] . "</option>");
				?>
			</select><br />
			
			<select class="selectpicker panel-select" name="country" data-width="100%" title="Country (required)" data-header="Country" required>
				<option value="<?php echo $m_row['country'] ?>" selected><?php echo $m_row['country'] ?></option>
				<?php
				// Select content from db to populate metro_area drop-down list
				$country_query = "SELECT * FROM menu_data_country ORDER BY country ASC";
				$country_result = $conn->query($country_query);
				
				// Output data of each row
				while($row = $country_result->fetch_assoc())
					echo("<option value='" . urlencode($row["country"]) . "'>" . $row["country"] . "</option>");
				?>
			</select><br />
			
			<select class="selectpicker panel-select" name="metro_area" data-width="100%" title="Metro Area (required)" data-header="Metro Area" required>
				<option value="<?php echo $m_row['metro_area'] ?>" selected><?php echo $m_row['metro_area'] ?></option>
				<?php
				// Select content from db to populate metro_area drop-down list
				$metro_area_query = "SELECT * FROM menu_data_metro_areas ORDER BY metro_area ASC";
				$metro_area_result = $conn->query($metro_area_query);

				// Output data of each row
				while($ma_row = $metro_area_result->fetch_assoc())
					echo("<option value='" . urlencode($ma_row["metro_area"]) . "'>" . $ma_row["metro_area"] . "</option>");
				?>
			</select><br />
			
			<input class="form-control panel-input" type="text" name="metro_area_city" value="<?php echo $m_row['metro_area_city']; ?>" placeholder="City in Metro Area" /><br />
			
			<input class="form-control panel-input" type="text" name="street" value="<?php echo $m_row['street']; ?>" placeholder="Street Mailing Address" /><br />
			
			<input class="form-control panel-input" type="text" name="city_state_country" value="<?php echo $m_row['city_state_country']; ?>" placeholder="City, State, Country" /><br />
			
			<input class="form-control panel-input" type="text" name="post_code" value="<?php echo $m_row['post_code']; ?>" placeholder="Post Code" /><br />
			
			<textarea class="form-control panel-textarea" rows="4" name="description" placeholder="Legal Practice Description and Other Background (3-4 sentences) (required)" required><?php echo $m_row['description']; ?></textarea><br />
			
			<!--
			<?php
			$testimonial_query_a = "SELECT * FROM testimonials WHERE group_id = '1' AND member_id = '$member_id'";
			$testimonial_result_a = $conn->query($testimonial_query_a);
			$testimonial_row_a = $testimonial_result_a->fetch_assoc();
			echo "<input class='form-control panel-input testimonial-inputs' type='text' name='testimonial' value='" . $testimonial_row_a["testimonial"] . "' placeholder='Testimonial' />";
			?>
			
			<div class="wrapper testimonial_wrapper">
				<div>
				<?php
				$testimonial_query_b = "SELECT * FROM testimonials WHERE group_id = '2' AND member_id = '$member_id' LIMIT 4";
				$testimonial_result_b = $conn->query($testimonial_query_b);
				$testimonial_counter = mysqli_num_rows($testimonial_result_b);
				while($testimonial_row_b = $testimonial_result_b->fetch_assoc()) {
					echo "<input class='form-control panel-input testimonial-inputs' type='text' name='testimonials[]' value='" . $testimonial_row_b["testimonial"] . "' placeholder='Testimonial' />";
				}
				?>
				</div>
			</div>
			
			<a class="add add_testimonial" href="javascript:void(0);">&#43; Additional Testimonials</a><br /><br />
			
			<?php
			$transaction_query_a = "SELECT * FROM transactions WHERE group_id = '1' AND member_id = '$member_id'";
			$transaction_result_a = $conn->query($transaction_query_a);
			$transaction_row_a = $transaction_result_a->fetch_assoc();
				echo "<input class='form-control panel-input transaction-inputs' type='text' name='transaction' value='" . $transaction_row_a["transaction"] . "' placeholder='Representative Matters / Transaction' />";
			?>
			
			<div class="wrapper rep_wrapper">
				<div>
				<?php
				$transaction_query_b = "SELECT * FROM transactions WHERE group_id = '2' AND member_id = '$member_id' LIMIT 4";
				$transaction_result_b = $conn->query($transaction_query_b);
				$transaction_counter = mysqli_num_rows($transaction_result_b);
				while($transaction_row_b = $transaction_result_b->fetch_assoc()) {
					echo "<input class='form-control panel-input transaction-inputs' type='text' name='testimonials[]' value='" . $transaction_row_b["transaction"] . "' placeholder='Representative Matters / Transaction' />";
				}
				?>
				</div>
			</div>
			
			<a class="add add_rep" href="javascript:void(0);">&#43; Additional Representative Matters / Transaction</a><br /><br />
			-->
			
			<select class="selectpicker panel-select" name="email_status" data-width="100%" multiple data-max-options="1" title="Discussion Board Email Notification (required)" data-header="Discussion Board Email Notification" required>
				<option value="<?php echo $m_row['email_status'] ?>" selected><?php echo $m_row['email_status'] ?></option>
				<option value="Enabled">Enabled</option>
				<option value="Disabled">Disabled</option>
			</select><br />
			<button id="panel-submit" type="submit" class="btn btn-primary" name="update">Update</button><br />
		</form>
	</div>
</div>

<script type="text/javascript">
// Update Profile
$(document).ready(function() {
	// Practice Areas
	$( "#practice_area_1" ).on( "change", function() {
        var paID = $(this).val();
        if(paID) {
            $.ajax({
                type: "POST",
                url: "fetch.php",
                data: "pa_id=" + paID,
                success: function(data) {
	                $( "#subcategory_1" ).selectpicker( "show" );
                    $( "#subcategory_1" ).html(data);
                    $( "#subcategory_1" ).selectpicker( "refresh" );      
				}
            }); 
        }
    });
    
	// Practice Areas
	$( "#practice_area_2" ).on( "change", function() {
        var paID = $(this).val();
        if(paID) {
            $.ajax({
                type: "POST",
                url: "fetch.php",
                data: "pa_id=" + paID,
                success: function(data) {
	                $( "#subcategory_2" ).selectpicker( "show" );
                    $( "#subcategory_2" ).html(data);
                     $( "#subcategory_2" ).selectpicker( "refresh" );             
				}
            }); 
        }
    });
    
	// Practice Areas
	$( "#practice_area_3" ).on( "change", function() {
        var paID = $(this).val();
        if(paID) {
            $.ajax({
                type: "POST",
                url: "fetch.php",
                data: "pa_id=" + paID,
                success: function(data) {
	                $( "#subcategory_3" ).selectpicker( "show" );
                    $( "#subcategory_3" ).html(data);   
                    $( "#subcategory_3" ).selectpicker( "refresh" );       
				}
            }); 
        }
    });
        
	// Testimonials and Representative Matters / Transaction
    var testimonialCounter = <?php echo $testimonial_counter; ?>;
    var addTestimonial = $( ".add_testimonial" ); //Add button selector
    var testimonialWrapper = $( ".testimonial_wrapper" ); //Input field wrapper
    var testimonialHTML = "<div><input class='form-control panel-input testimonial-inputs' type='text' name='testimonials[]' value='<?php echo $testimonial_row_b["testimonial"] ?>' /><a href='javascript:void(0);' class='remove remove_button' title='Remove Field'>- Remove Testimonial</a></div>"; //New input field html 
	//Initial field counter is 1
    $(addTestimonial).click(function(){ //Once add button is clicked
        if (testimonialCounter == 4) { //Check maximum number of input fields
            $(addTestimonial).hide();
        }
        else {
	        testimonialCounter++; //Increment field counter
            $(testimonialWrapper).append(testimonialHTML); // Add field html
        }
    });
    $(testimonialWrapper).on("click", ".remove_button", function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent("div").remove(); //Remove field html
        testimonialCounter--; //Decrement field counter
        if (testimonialCounter < 4){
		    $(addTestimonial).show();
	    }
    });
 
	var transactionCounter = <?php echo $transaction_counter; ?>;
    var addRep = $( ".add_rep" ); //Add button selector
    var repWrapper = $( ".rep_wrapper" ); //Input field wrapper
    var repHTML = "<div><input class='form-control panel-input transaction-inputs' type='text' name='transactions[]' value='<?php echo $transaction_row_b["transaction"] ?>' /><a href='javascript:void(0);' class='remove remove_button' title='- Remove Field'>- Remove Representative Matter / Transaction</a></div>"; //New input field html 
    $(addRep).click(function(){ //Once add button is clicked
        if(transactionCounter == 4) { //Check maximum number of input fields
            $(addRep).hide();
        }
        else {
	    	transactionCounter++; //Increment field counter
		   $(repWrapper).append(repHTML); // Add field html 
        }
    });
    $(repWrapper).on("click", ".remove_button", function(e){ //Once remove button is clicked
	    e.preventDefault();
        $(this).parent("div").remove(); //Remove field html
        transactionCounter--; //Decrement field counter
	    if (transactionCounter < 4){
		    $(addRep).show();
	    }
    });
});
</script>

<?php
}
else
	echo "<script>window.open('../member_login.php','_self')</script>";

require_once("assets/includes/footer.php");
?>